**M3**- Programació                                         

Eric Santos Cortada                                                                                             

Professor/a: Toni Pifarré Mata

Aquesta pràctica tracta d'introduir un text, fer una simulació de compactació i mostrar-la per pantalla, seguidament mostrar informació de la compactació i una taula amb els diferents caràcters ordenats per repeticions amb les codis ascendentment. Després reintroduir el text compactat i mostrar-lo descompactat.Tot això amb AiF(accions i funcions).

```
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>

#define MAX_TEXT 100
#define MAX_TEXT2 200
#define MAX_TEXT3 200
#define MAX_REPETICIONS 100
#define MAX_CARACTERS 100
#define MAX_CODIS 100
#define MAX_DIGITS 4
#define BuidaBuffer while(getchar()!='\n')

void longitudText(char text[],int *l);
void guardarRepeticions (char text[],char caracter[], int repeticions[]);
void ordenarRepeticions (char caracter[],char codis[MAX_CODIS][MAX_DIGITS],int repeticions[]);
void codificarText (char text[],char text2[],char caracter[], char codis[MAX_CODIS][MAX_DIGITS]);
void infoBits(char text[], int *l, char text2[]);
void imprimirRepeticions(char caracter[],char codis[MAX_CODIS][MAX_DIGITS],int repeticions[]);
void llegirCodi(char text2[], int *it2, char codiCompactat[]);
bool iguals(char par1[], char par2[]);
void descodificarText(char codiCompactat[],char codis[MAX_CODIS][MAX_DIGITS],char text2[], int *it2, char text3[], char caracter[]);

int main()
{
    char text[MAX_TEXT+1];
    char text2[MAX_TEXT2+1];
    char text3[MAX_TEXT3+1];
    char codiCompactat[MAX_DIGITS];
    char caracter[MAX_CARACTERS+1];
    int repeticions[MAX_REPETICIONS+1];
    int it,it2,it3,l;
    char codis[MAX_CODIS][MAX_DIGITS]= {"0","1","00","01","10","11","000","001","010","011","100","101","110","111","0000",
                                        "0001","0010","0011","0100","0101","0110","0111","1000","1001","1010","1011","1100",
                                        "1101","1110","1111",""};

    //INICIALITZACIONS
    it=0;
    it2=0;
    it3=0;
    l=0;
    text[it]='\0';
    text2[it2]='\0';
    text3[it3]='\0';

    for(it=0;it<=MAX_CARACTERS;it++)    //inicialitzar caracter
        caracter[it]='\0';

    for(it=0;it<MAX_REPETICIONS;it++)   //inicialitzar repeticions
        repeticions[it]=1;

    it=0;

    printf("Introdueix un text:\n       ");
    scanf("%100[^\n]",text);

    guardarRepeticions (text,caracter,repeticions);
    ordenarRepeticions (caracter,codis,repeticions);
    codificarText (text,text2,caracter,codis);

    infoBits(text,&l,text2);

    printf("   caracter \trepeticions \tcodi assignat\n");
    imprimirRepeticions(caracter,codis,repeticions);

    BuidaBuffer;

    printf("\nIntrodueix el text compactat:\n\t");
    scanf("%100[^\n]",text2);

    descodificarText(codiCompactat,codis,text2,&it2,text3,caracter);

    return 0;
}

void longitudText(char text[],int *l){
    while(text[*l]!='\0') (*l)++;

}

void guardarRepeticions (char text[],char caracter[], int repeticions[]){
    int it=0;
    while(text[it]!='\0'){
        int ic=0;
        while(text[it]!=caracter[ic] &&
              caracter[ic]!='\0') ic++;
        if(caracter[ic]=='\0'){
            caracter[ic]=text[it];
        }else{
            repeticions[ic]++;
        }
        it++;
    }
}

void ordenarRepeticions (char caracter[],char codis[MAX_CODIS][MAX_DIGITS],int repeticions[]){
    char auxchar;
    int j=0;
    int ic=0;
    int i=0;
    int aux=0;
    while(caracter[ic]!='\0'){
        ic++;
    }
    for(i=2;i<=ic;i++){
        for(j=0;j<=ic-i;j++){
            if(repeticions[j]<repeticions[j+1]) {
                aux=repeticions[j];
                repeticions[j]=repeticions[j+1]; //metode bombolla
                repeticions[j+1]=aux;
                auxchar=caracter[j];
                caracter[j]=caracter[j+1];
                caracter[j+1]=auxchar;
            }
        }
    }
}

void codificarText (char text[],char text2[],char caracter[], char codis[MAX_CODIS][MAX_DIGITS]){
    int i=0;
    int it=0;
    int it2=0;
    int icodi=0;
    char auxchar='\0';

    while(text[it]!='\0'){
        auxchar=text[it];//copiar caracter de text per anar a per el codi
        i=0;
        while(auxchar!=caracter[i])i++;//buscar codi del caracter que hem agafat
        icodi=0;
        if(auxchar==caracter[i]){
            while(codis[i][icodi]!='\0'){
                text2[it2]=codis[i][icodi];//copiar codi al text auxiliar per a poder mostrar la codificació
                it2++;icodi++;
            }
        }
        text2[it2]='$';
        it2++;
        it++;
    }

    //trere $ del final
    it2--;
    text2[it2]='\0';

    printf("\nText compactat: \n       %s\n\n",text2);
}

void infoBits(char text[], int *l, char text2[]){
    longitudText (text,l);
    int it=0;
    int it2=0;

    while(text2[it]!='\0'){
        while(text2[it]!='$' && text2[it]!='\0'){
            it2++;it++;
        }
        while(text2[it]=='$'){
            it++;
        }
    }

    printf("\nBits del text: %d*8 = %d bits\n",*l,(*l)*8);
    printf("Bits del text compactat: %d bits\n",it2);
    printf("%% de compactacio: %d%%\n\n",100-((it2*100)/((*l)*8)));
}

void imprimirRepeticions(char caracter[],char codis[MAX_CODIS][MAX_DIGITS],int repeticions[]){
        int i=0;
        int ic=0;
        while(caracter[ic]!='\0') ic++;
        while(i<ic){
        printf("      %c      ->   %d cop/s    ->      %s\n",caracter[i],repeticions[i],codis[i]);
            i++;
    }
}

void llegirCodi(char text2[], int *it2, char codiCompactat[]){
    int i=0;
    while(text2[*it2]!='$' && text2[*it2]!='\0'){
        codiCompactat[i]=text2[*it2];
        i++;
        (*it2)++;
    }
    codiCompactat[i]='\0';
    //saltar marca final de codi, el '$'
    if(text2[*it2]=='$') (*it2)++;
}

bool iguals(char par1[], char par2[]){
    int i=0;
    while (par1[i]==par2[i] && par1[i]!='\0'){
        i++;
    }

    return par1[i]==par2[i];
}

void descodificarText(char codiCompactat[],char codis[MAX_CODIS][MAX_DIGITS],char text2[], int *it2, char text3[], char caracter[]){
    int i=0;
    int it3=0;
    while(text2[*it2]!='\0'){
        i=0;
        llegirCodi(text2,it2,codiCompactat);
        while(codis[i][0]!='\0'){
            if(iguals(codiCompactat,codis[i])){
                break;
            }
            i++;
        }
        text3[it3]=caracter[i];
        it3++;
    }
    text3[it3]='\0';

    printf("\nEl text descompactat es:\n\t%s\n",text3);
}

```
![](Pràctica_UF2.png)
